﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IESOK.Models;

namespace IESOK.Controllers
{
    public class SJMsController : Controller
    {
        private KUBAEntities db = new KUBAEntities();

        // GET: SJMs
        public ActionResult Index()
        {
            return View(db.SJM.ToList());
        }

        // GET: SJMs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SJM sJM = db.SJM.Find(id);
            if (sJM == null)
            {
                return HttpNotFound();
            }
            return View(sJM);
        }

        // GET: SJMs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SJMs/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SJMID,NAZWA")] SJM sJM)
        {
            if (ModelState.IsValid)
            {
                db.SJM.Add(sJM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sJM);
        }

        // GET: SJMs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SJM sJM = db.SJM.Find(id);
            if (sJM == null)
            {
                return HttpNotFound();
            }
            return View(sJM);
        }

        // POST: SJMs/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SJMID,NAZWA")] SJM sJM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sJM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sJM);
        }

        // GET: SJMs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SJM sJM = db.SJM.Find(id);
            if (sJM == null)
            {
                return HttpNotFound();
            }
            return View(sJM);
        }

        // POST: SJMs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SJM sJM = db.SJM.Find(id);
            db.SJM.Remove(sJM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
