﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IESOK.Models;

namespace IESOK.Controllers
{
    public class SFPsController : Controller
    {
        private KUBAEntities db = new KUBAEntities();

        // GET: SFPs
        public ActionResult Index()
        {
            return View(db.SFP.ToList());
        }

        // GET: SFPs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SFP sFP = db.SFP.Find(id);
            if (sFP == null)
            {
                return HttpNotFound();
            }
            return View(sFP);
        }

        // GET: SFPs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SFPs/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDSFP,NAZWA,TERMIN,PROCENTP,RODZAJ")] SFP sFP)
        {
            if (ModelState.IsValid)
            {
                db.SFP.Add(sFP);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sFP);
        }

        // GET: SFPs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SFP sFP = db.SFP.Find(id);
            if (sFP == null)
            {
                return HttpNotFound();
            }
            return View(sFP);
        }

        // POST: SFPs/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDSFP,NAZWA,TERMIN,PROCENTP,RODZAJ")] SFP sFP)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sFP).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sFP);
        }

        // GET: SFPs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SFP sFP = db.SFP.Find(id);
            if (sFP == null)
            {
                return HttpNotFound();
            }
            return View(sFP);
        }

        // POST: SFPs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SFP sFP = db.SFP.Find(id);
            db.SFP.Remove(sFP);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
