﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IESOK.Models;

namespace IESOK.Controllers
{
    public class DOKsController : Controller
    {
        private KUBAEntities db = new KUBAEntities();

        // GET: DOKs
        public ActionResult Index()
        {
            var dOK = db.DOK.Include(d => d.MAG).Include(d => d.SFP);
            return View(dOK.ToList());
        }

        // GET: DOKs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DOK dOK = db.DOK.Find(id);
            if (dOK == null)
            {
                return HttpNotFound();
            }
            return View(dOK);
        }

        // GET: DOKs/Create
        public ActionResult Create()
        {
            ViewBag.MAGID = new SelectList(db.MAG, "MAGID", "NAZWA");
            ViewBag.SFPID = new SelectList(db.SFP, "IDSFP", "NAZWA");
            ViewBag.KKHID = new SelectList(db.KKH, "IDKKH", "NAZWA");
            

            return View();
        }

        // POST: DOKs/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DOKID,DATA,SDOID,SFPID,KKHID,MAGID,NUMER,DATA_D,DATA_T,DATA_P,DATA_Z,WARTOSCN,WARTOSCP,WARTOSCB,WARTOSCE,WARTOSCZ,OPIS,TYP,FLAGA,NOTA,RO,STACJA")] DOK dOK)
        {
            if (ModelState.IsValid)
            {
                db.DOK.Add(dOK);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MAGID = new SelectList(db.MAG, "MAGID", "NAZWA", dOK.MAGID);
            ViewBag.SFPID = new SelectList(db.SFP, "IDSFP", "NAZWA", dOK.SFPID);
            ViewBag.KKHID = new SelectList(db.KKH, "IDKKH", "NAZWA", dOK.KKHID);
            return View(dOK);
        }

        // GET: DOKs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DOK dOK = db.DOK.Find(id);
            if (dOK == null)
            {
                return HttpNotFound();
            }
            ViewBag.MAGID = new SelectList(db.MAG, "MAGID", "NAZWA", dOK.MAGID);
            ViewBag.SFPID = new SelectList(db.SFP, "IDSFP", "NAZWA", dOK.SFPID);
            ViewBag.KKHID = new SelectList(db.KKH, "IDKKH", "NAZWA", dOK.KKHID);
            return View(dOK);
        }

        // POST: DOKs/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DOKID,DATA,SDOID,SFPID,KKHID,MAGID,NUMER,DATA_D,DATA_T,DATA_P,DATA_Z,WARTOSCN,WARTOSCP,WARTOSCB,WARTOSCE,WARTOSCZ,OPIS,TYP,FLAGA,NOTA,RO,STACJA")] DOK dOK)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dOK).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MAGID = new SelectList(db.MAG, "MAGID", "NAZWA", dOK.MAGID);
            ViewBag.SFPID = new SelectList(db.SFP, "IDSFP", "NAZWA", dOK.SFPID);
            ViewBag.KKHID = new SelectList(db.KKH, "IDKKH", "NAZWA", dOK.KKHID);
            return View(dOK);
        }

        // GET: DOKs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DOK dOK = db.DOK.Find(id);
            if (dOK == null)
            {
                return HttpNotFound();
            }
            return View(dOK);
        }

        // POST: DOKs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DOK dOK = db.DOK.Find(id);
            db.DOK.Remove(dOK);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
