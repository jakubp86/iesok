﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IESOK.Models;

namespace IESOK.Controllers
{
    public class MAGsController : Controller
    {
        private KUBAEntities db = new KUBAEntities();

        // GET: MAGs1
        public ActionResult Index()
        {
            return View(db.MAG.ToList());
        }

        // GET: MAGs1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MAG mAG = db.MAG.Find(id);
            if (mAG == null)
            {
                return HttpNotFound();
            }
            return View(mAG);
        }

        // GET: MAGs1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MAGs1/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MAGID,NAZWA,KOD,KONTO_N,KONTO_P")] MAG mAG)
        {
            if (ModelState.IsValid)
            {
                db.MAG.Add(mAG);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mAG);
        }

        // GET: MAGs1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MAG mAG = db.MAG.Find(id);
            if (mAG == null)
            {
                return HttpNotFound();
            }
            return View(mAG);
        }

        // POST: MAGs1/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MAGID,NAZWA,KOD,KONTO_N,KONTO_P")] MAG mAG)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mAG).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mAG);
        }

        // GET: MAGs1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MAG mAG = db.MAG.Find(id);
            if (mAG == null)
            {
                return HttpNotFound();
            }
            return View(mAG);
        }

        // POST: MAGs1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MAG mAG = db.MAG.Find(id);
            db.MAG.Remove(mAG);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
