﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IESOK.Models;

namespace IESOK.Controllers
{
    public class SATsController : Controller
    {
        private KUBAEntities db = new KUBAEntities();

        // GET: SATs
        public ActionResult Index()
        {
            return View(db.SAT.ToList());
        }

        // GET: SATs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SAT sAT = db.SAT.Find(id);
            if (sAT == null)
            {
                return HttpNotFound();
            }
            return View(sAT);
        }

        // GET: SATs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SATs/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDSAT,NAZWAS,TYP")] SAT sAT)
        {
            if (ModelState.IsValid)
            {
                db.SAT.Add(sAT);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sAT);
        }

        // GET: SATs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SAT sAT = db.SAT.Find(id);
            if (sAT == null)
            {
                return HttpNotFound();
            }
            return View(sAT);
        }

        // POST: SATs/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDSAT,NAZWAS,TYP")] SAT sAT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sAT).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sAT);
        }

        // GET: SATs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SAT sAT = db.SAT.Find(id);
            if (sAT == null)
            {
                return HttpNotFound();
            }
            return View(sAT);
        }

        // POST: SATs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SAT sAT = db.SAT.Find(id);
            db.SAT.Remove(sAT);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
