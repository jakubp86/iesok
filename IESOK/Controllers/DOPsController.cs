﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IESOK.Models;

namespace IESOK.Controllers
{
    public class DOPsController : Controller
    {
        private KUBAEntities db = new KUBAEntities();

        // GET: DOPs
        public ActionResult Index()
        {
            var dOP = db.DOP.Include(d => d.SJM).Include(d => d.STW).Include(d => d.SVT).Include(d => d.DOK);
            return View(dOP.ToList());
        }

        // GET: DOPs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DOP dOP = db.DOP.Find(id);
            if (dOP == null)
            {
                return HttpNotFound();
            }
            return View(dOP);
        }

        // GET: DOPs/Create
        public ActionResult Create()
        {
            ViewBag.SJMID = new SelectList(db.SJM, "SJMID", "NAZWA");
            ViewBag.STWID = new SelectList(db.STW, "STWID", "NAZWA");
            ViewBag.SVTID = new SelectList(db.SVT, "SVTID", "NAZWA");
            ViewBag.DOKID = new SelectList(db.DOK, "DOKID", "NUMER");
           
            return View();
        }

        // POST: DOPs/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DOPID,DOKID,STWID,SJMID,SVTID,NAZWA,OPIS,ILOSC,CENAN,CENAB,WARTOSCN,WARTOSCB,WARTOSCE,CENAE,RABAT,CENANP,CENABP,RO")] DOP dOP)
        {
            if (ModelState.IsValid)
            {
                db.DOP.Add(dOP);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SJMID = new SelectList(db.SJM, "SJMID", "NAZWA", dOP.SJMID);
            ViewBag.STWID = new SelectList(db.STW, "STWID", "NAZWA", dOP.STWID);
            ViewBag.SVTID = new SelectList(db.SVT, "SVTID", "NAZWA", dOP.SVTID);
            ViewBag.DOKID = new SelectList(db.DOK, "DOKID", "NUMER", dOP.DOKID);
            return View(dOP);
        }

        // GET: DOPs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DOP dOP = db.DOP.Find(id);
            if (dOP == null)
            {
                return HttpNotFound();
            }
            ViewBag.SJMID = new SelectList(db.SJM, "SJMID", "NAZWA", dOP.SJMID);
            ViewBag.STWID = new SelectList(db.STW, "STWID", "NAZWA", dOP.STWID);
            ViewBag.SVTID = new SelectList(db.SVT, "SVTID", "NAZWA", dOP.SVTID);
            ViewBag.DOKID = new SelectList(db.DOK, "DOKID", "NUMER", dOP.DOKID);
            return View(dOP);
        }

        // POST: DOPs/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DOPID,DOKID,STWID,SJMID,SVTID,NAZWA,OPIS,ILOSC,CENAN,CENAB,WARTOSCN,WARTOSCB,WARTOSCE,CENAE,RABAT,CENANP,CENABP,RO")] DOP dOP)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dOP).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SJMID = new SelectList(db.SJM, "SJMID", "NAZWA", dOP.SJMID);
            ViewBag.STWID = new SelectList(db.STW, "STWID", "NAZWA", dOP.STWID);
            ViewBag.SVTID = new SelectList(db.SVT, "SVTID", "NAZWA", dOP.SVTID);
            ViewBag.DOKID = new SelectList(db.DOK, "DOKID", "NUMER", dOP.DOKID);
            return View(dOP);
        }

        // GET: DOPs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DOP dOP = db.DOP.Find(id);
            if (dOP == null)
            {
                return HttpNotFound();
            }
            return View(dOP);
        }

        // POST: DOPs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DOP dOP = db.DOP.Find(id);
            db.DOP.Remove(dOP);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
