﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IESOK.Models;

namespace IESOK.Controllers
{
    public class STWsController : Controller
    {
        private KUBAEntities db = new KUBAEntities();

        // GET: STWs
        public ActionResult Index()
        {
            var sTW = db.STW.Include(s => s.MAG).Include(s => s.SGT).Include(s => s.SJM).Include(s => s.SVT);
            return View(sTW.ToList());
        }

        // GET: STWs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STW sTW = db.STW.Find(id);
            if (sTW == null)
            {
                return HttpNotFound();
            }
            return View(sTW);
        }

        // GET: STWs/Create
        public ActionResult Create()
        {
            ViewBag.MAGID = new SelectList(db.MAG, "MAGID", "NAZWA");
            ViewBag.SGTID = new SelectList(db.SGT, "IDSGT", "NAZWAG");
            ViewBag.SJMID = new SelectList(db.SJM, "SJMID", "NAZWA");
            ViewBag.SVTID = new SelectList(db.SVT, "SVTID", "NAZWA");
            return View();
        }

        // POST: STWs/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "STWID,SGTID,SJMID,SVTID,SMB,KEAN,NAZWA,OPIS,CN,CB,FLAGA,PKWIU,STANTMP,MAGID,OCENAZ,NOTA,DLUG,SZER,WAGA,CECHA1,CECHA2,CECHA3,SPIS,CZAST,SKROT,NRPRODUCT,PRZELICZNIK,AKCYZA")] STW sTW)
        {
            if (ModelState.IsValid)
            {
                db.STW.Add(sTW);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MAGID = new SelectList(db.MAG, "MAGID", "NAZWA", sTW.MAGID);
            ViewBag.SGTID = new SelectList(db.SGT, "IDSGT", "NAZWAG", sTW.SGTID);
            ViewBag.SJMID = new SelectList(db.SJM, "SJMID", "NAZWA", sTW.SJMID);
            ViewBag.SVTID = new SelectList(db.SVT, "SVTID", "NAZWA", sTW.SVTID);
            return View(sTW);
        }

        // GET: STWs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STW sTW = db.STW.Find(id);
            if (sTW == null)
            {
                return HttpNotFound();
            }
            ViewBag.MAGID = new SelectList(db.MAG, "MAGID", "NAZWA", sTW.MAGID);
            ViewBag.SGTID = new SelectList(db.SGT, "IDSGT", "NAZWAG", sTW.SGTID);
            ViewBag.SJMID = new SelectList(db.SJM, "SJMID", "NAZWA", sTW.SJMID);
            ViewBag.SVTID = new SelectList(db.SVT, "SVTID", "NAZWA", sTW.SVTID);
            return View(sTW);
        }

        // POST: STWs/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "STWID,SGTID,SJMID,SVTID,SMB,KEAN,NAZWA,OPIS,CN,CB,FLAGA,PKWIU,STANTMP,MAGID,OCENAZ,NOTA,DLUG,SZER,WAGA,CECHA1,CECHA2,CECHA3,SPIS,CZAST,SKROT,NRPRODUCT,PRZELICZNIK,AKCYZA")] STW sTW)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sTW).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MAGID = new SelectList(db.MAG, "MAGID", "NAZWA", sTW.MAGID);
            ViewBag.SGTID = new SelectList(db.SGK, "IDSGT", "NAZWAG", sTW.SGTID);
            ViewBag.SJMID = new SelectList(db.SJM, "SJMID", "NAZWA", sTW.SJMID);
            ViewBag.SVTID = new SelectList(db.SVT, "SVTID", "NAZWA", sTW.SVTID);
            return View(sTW);
        }

        // GET: STWs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            STW sTW = db.STW.Find(id);
            if (sTW == null)
            {
                return HttpNotFound();
            }
            return View(sTW);
        }

        // POST: STWs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            STW sTW = db.STW.Find(id);
            db.STW.Remove(sTW);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
