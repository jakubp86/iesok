﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IESOK.Models;

namespace IESOK.Controllers
{
    public class SVTsController : Controller
    {
        private KUBAEntities db = new KUBAEntities();

        // GET: SVTs
        public ActionResult Index()
        {
            return View(db.SVT.ToList());
        }

        // GET: SVTs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SVT sVT = db.SVT.Find(id);
            if (sVT == null)
            {
                return HttpNotFound();
            }
            return View(sVT);
        }

        // GET: SVTs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SVTs/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SVTID,NAZWA,KODP,KODF,VAT,REJESTR,EDYCJA,STATUS")] SVT sVT)
        {
            if (ModelState.IsValid)
            {
                db.SVT.Add(sVT);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sVT);
        }

        // GET: SVTs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SVT sVT = db.SVT.Find(id);
            if (sVT == null)
            {
                return HttpNotFound();
            }
            return View(sVT);
        }

        // POST: SVTs/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SVTID,NAZWA,KODP,KODF,VAT,REJESTR,EDYCJA,STATUS")] SVT sVT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sVT).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sVT);
        }

        // GET: SVTs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SVT sVT = db.SVT.Find(id);
            if (sVT == null)
            {
                return HttpNotFound();
            }
            return View(sVT);
        }

        // POST: SVTs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SVT sVT = db.SVT.Find(id);
            db.SVT.Remove(sVT);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
