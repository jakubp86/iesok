﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IESOK.Models;

namespace IESOK.Controllers
{
    public class SGTsController : Controller
    {
        private KUBAEntities db = new KUBAEntities();

        // GET: SGTs
        public ActionResult Index()
        {
            var sGT = db.SGT.Include(s => s.SAT);
            return View(sGT.ToList());
        }

        // GET: SGTs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SGT sGT = db.SGT.Find(id);
            if (sGT == null)
            {
                return HttpNotFound();
            }
            return View(sGT);
        }

        // GET: SGTs/Create
        public ActionResult Create()
        {
            ViewBag.ID_SAT = new SelectList(db.SAT, "IDSAT", "NAZWAS");
            return View();
        }

        // POST: SGTs/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDSGT,ID_SAT,NAZWAG")] SGT sGT)
        {
            if (ModelState.IsValid)
            {
                db.SGT.Add(sGT);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_SAT = new SelectList(db.SAT, "IDSAT", "NAZWAS", sGT.ID_SAT);
            return View(sGT);
        }

        // GET: SGTs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SGT sGT = db.SGT.Find(id);
            if (sGT == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_SAT = new SelectList(db.SAT, "IDSAT", "NAZWAS", sGT.ID_SAT);
            return View(sGT);
        }

        // POST: SGTs/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDSGT,ID_SAT,NAZWAG")] SGT sGT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sGT).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_SAT = new SelectList(db.SAT, "IDSAT", "NAZWAS", sGT.ID_SAT);
            return View(sGT);
        }

        // GET: SGTs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SGT sGT = db.SGT.Find(id);
            if (sGT == null)
            {
                return HttpNotFound();
            }
            return View(sGT);
        }

        // POST: SGTs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SGT sGT = db.SGT.Find(id);
            db.SGT.Remove(sGT);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
