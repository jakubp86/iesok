﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IESOK.Models;

namespace IESOK.Controllers
{
    public class KTWsController : Controller
    {
        private KUBAEntities db = new KUBAEntities();

        // GET: KTWs
        public ActionResult Index()
        {
            var kTW = db.KTW.Include(k => k.DOP).Include(k => k.STW);
            return View(kTW.ToList());
        }

        // GET: KTWs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KTW kTW = db.KTW.Find(id);
            if (kTW == null)
            {
                return HttpNotFound();
            }
            return View(kTW);
        }

        // GET: KTWs/Create
        public ActionResult Create()
        {
            ViewBag.DOPID = new SelectList(db.DOP, "DOPID", "NAZWA");
            ViewBag.STWID = new SelectList(db.STW, "STWID", "SMB");
            return View();
        }

        // POST: KTWs/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KTWID,DOPID,STWID,DATA,CENA,ILOSCP,ILOSCR,STAN")] KTW kTW)
        {
            if (ModelState.IsValid)
            {
                db.KTW.Add(kTW);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DOPID = new SelectList(db.DOP, "DOPID", "NAZWA", kTW.DOPID);
            ViewBag.STWID = new SelectList(db.STW, "STWID", "SMB", kTW.STWID);
            return View(kTW);
        }

        // GET: KTWs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KTW kTW = db.KTW.Find(id);
            if (kTW == null)
            {
                return HttpNotFound();
            }
            ViewBag.DOPID = new SelectList(db.DOP, "DOPID", "NAZWA", kTW.DOPID);
            ViewBag.STWID = new SelectList(db.STW, "STWID", "SMB", kTW.STWID);
            return View(kTW);
        }

        // POST: KTWs/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KTWID,DOPID,STWID,DATA,CENA,ILOSCP,ILOSCR,STAN")] KTW kTW)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kTW).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DOPID = new SelectList(db.DOP, "DOPID", "NAZWA", kTW.DOPID);
            ViewBag.STWID = new SelectList(db.STW, "STWID", "SMB", kTW.STWID);
            return View(kTW);
        }

        // GET: KTWs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KTW kTW = db.KTW.Find(id);
            if (kTW == null)
            {
                return HttpNotFound();
            }
            return View(kTW);
        }

        // POST: KTWs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KTW kTW = db.KTW.Find(id);
            db.KTW.Remove(kTW);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
