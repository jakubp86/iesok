﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IESOK.Models;

namespace IESOK.Controllers
{
    public class KKHsController : Controller
    {
        private KUBAEntities db = new KUBAEntities();

        // GET: KKHs
        public ActionResult Index()
        {
            return View(db.KKH.ToList());
        }

        // GET: KKHs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KKH kKH = db.KKH.Find(id);
            if (kKH == null)
            {
                return HttpNotFound();
            }
            return View(kKH);
        }

        // GET: KKHs/Create
        public ActionResult Create()
        {
            ViewBag.IDSFP = new SelectList(db.SFP, "IDSFP", "NAZWA");
            ViewBag.IDSGK = new SelectList(db.SGK, "IDSGK", "NAZWA");
            return View();
        }

        // POST: KKHs/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDKKH,ID_SFP,KOD,SYNONIM,NAZWA,NAZWA2,ADRES,ADRES2,NIP,REGON,ADRES_EMAIL,ADRES_WWW,BANK_A,BANK_N,TELEFONY,INNE,KODPOCZTOWY,MIASTO,KONTO,ODBIERA,NOTA,DOJ_KM,DOJ_CZAS,DOJ_RYCZ,TYM,ID_SGK,DATA,RABAT,TYP,ACT,AKCYZA,FIRMA")] KKH kKH)
        {
            if (ModelState.IsValid)
            {
                db.KKH.Add(kKH);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kKH);
        }

        // GET: KKHs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KKH kKH = db.KKH.Find(id);
            if (kKH == null)
            {
                return HttpNotFound();
            }
            return View(kKH);
        }

        // POST: KKHs/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDKKH,ID_SFP,KOD,SYNONIM,NAZWA,NAZWA2,ADRES,ADRES2,NIP,REGON,ADRES_EMAIL,ADRES_WWW,BANK_A,BANK_N,TELEFONY,INNE,KODPOCZTOWY,MIASTO,KONTO,ODBIERA,NOTA,DOJ_KM,DOJ_CZAS,DOJ_RYCZ,TYM,ID_SGK,DATA,RABAT,TYP,ACT,AKCYZA,FIRMA")] KKH kKH)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kKH).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kKH);
        }

        // GET: KKHs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KKH kKH = db.KKH.Find(id);
            if (kKH == null)
            {
                return HttpNotFound();
            }
            return View(kKH);
        }

        // POST: KKHs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KKH kKH = db.KKH.Find(id);
            db.KKH.Remove(kKH);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
