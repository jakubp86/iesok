﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IESOK.Startup))]
namespace IESOK
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
